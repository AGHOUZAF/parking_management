import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isScrolled = false;
  darkMode = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const scrollPosition = window.pageYOffset;
    this.isScrolled = scrollPosition >= 10;
  }

  ngOnInit() {
    setTimeout(() => {
      document.body.classList.remove('hero-anime');
    }, 1000);
  }

  toggleTheme() {
    this.darkMode = !this.darkMode;
    if (this.darkMode) {
      document.body.classList.add('dark');
      const switchElement = document.getElementById('switch');
      if (switchElement) {
        switchElement.classList.add('switched');
      }
    } else {
      document.body.classList.remove('dark');
      const switchElement = document.getElementById('switch');
      if (switchElement) {
        switchElement.classList.remove('switched');
      }
    }
  }
}
